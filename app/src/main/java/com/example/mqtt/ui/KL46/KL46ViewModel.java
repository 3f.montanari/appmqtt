package com.example.mqtt.ui.KL46;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class KL46ViewModel extends ViewModel {

    private final MutableLiveData<String> mText;

    public KL46ViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is home fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}